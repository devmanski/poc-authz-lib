package com.lantana.school.course.coursemanagment;

import java.util.List;
import org.keycloak.authorization.client.AuthzClient;
import org.keycloak.representations.idm.authorization.AuthorizationRequest;
import org.keycloak.representations.idm.authorization.AuthorizationResponse;
import org.springframework.stereotype.Service;

@Service
public class UmaService {

  public String getRptToken(String token, String resource, List<String> scopes) {
    AuthzClient authzClient = AuthzClient.create();
    AuthorizationRequest request = new AuthorizationRequest();
    request.addPermission(resource, scopes);
    AuthorizationResponse response = authzClient.authorization(token).authorize(request);
    return response.getToken();
  }

}
