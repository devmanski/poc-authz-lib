package com.lantana.school.course.coursemanagment;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UmaController {

  private final UmaService umaService;

  public UmaController(UmaService umaService) {
    this.umaService = umaService;
  }

  @PreAuthorize("hasPermission(#resource ,'elemntaryX,elemntaryZ')")
  @GetMapping(value = "/test-uma", produces = MediaType.APPLICATION_JSON_VALUE)
  public String test(@RequestParam String resource) {
    return "test";
  }
}
