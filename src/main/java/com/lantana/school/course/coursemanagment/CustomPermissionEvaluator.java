package com.lantana.school.course.coursemanagment;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.keycloak.KeycloakPrincipal;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
public class CustomPermissionEvaluator implements PermissionEvaluator {

  private UmaService umaService;

  public CustomPermissionEvaluator(UmaService umaService) {
    this.umaService = umaService;
  }

  @Override
  public boolean hasPermission(Authentication authentication, Object o, Object o1) {
    if (o != null && o1 != null && authentication.getPrincipal() instanceof KeycloakPrincipal) {
      try {
        KeycloakPrincipal keycloakPrincipal = (KeycloakPrincipal) authentication.getPrincipal();
        String token = keycloakPrincipal.getKeycloakSecurityContext().getTokenString();
        umaService.getRptToken(token, o.toString(), getList(o1.toString()));
        return true;
      }catch (Exception e){
        return false;
      }
    }
    return false;
  }

  @Override
  public boolean hasPermission(Authentication authentication, Serializable serializable, String s,
      Object o) {
    return false;
  }


  private List<String> getList(String string) {
    return Stream.of(string.split(","))
        .map(String::trim)
        .collect(Collectors.toList());
  }


}
